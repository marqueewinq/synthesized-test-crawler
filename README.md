# async-test-crawler

This is a test assignment for the coding interview (2020).

## Problem statement

Write an asynchronous web page crawler. Upon `KeyboardInterrupt` the crawler should stop; it should be possible to launch it again later on from that point (instead of fetching everything from the start).

## Setup & Installation

List of files:

 - `crawler.py` : crawler script (see `python3 crawler.py --help`)
 - `test_crawler.py` : pytest tests for crawler.py
 - `input.csv` : demo csv seedfile

Installation:

```
virtualenv env .
source env/bin/activate
pip install -r requirements.txt
python3 crawler.py input.csv output.csv --max-depth=1
```

Run the tests (inside virtualenv):

```
python3 -m pytest
```

## Solution

When working with the asynchronous functions it is vital to keep transactional design. Conditional 
we keep the script's design in state-action-state paradigm, handling exceptions should be 
no problem: we save the state on the hard drive between actions and catch the KeyboardInterrupt in 
the main thread. Inner functions must take care of handling their exceptions (see `_http_request`).

The state is:

 - `to_fetch` : list of urls to fetch
 - `seen_urls` : set of already seen urls
 - `current_depth` : current depth (batch number) of crawling
 - `results` : fetched results (list of dicts)

The action is:

 - Take `to_fetch` and perform fetches
 - Parse fetched pages and obtain new urls
 - Update the state:
    - `to_fetch` is set to new urls
    - increment the `current_depth`
    - update `seen_urls` and `results` accordingly.

When the scripts receives a KeyboardInterrupt, it will save the current state to the disk and resume
from it on the next launch.

## ToDo

 - If you've run the tests, you've probably noticed that they create a warnings about async objects
   created outside of async functions. These are fine, but we might wish to interface actual
   functions from async ones to remove them. I've avoided that to keep the code simple and readable.

```
def async_func_body():
    # do actual work here

async def async_func():
    return async_func_body()
```

 - Simple save/load from file on the hard drive is fine in the case of this simple script, but it's
   better to use actual database engines (i.e. Postgres or Mongo) to do transactions -- as in, for
   example, `celery` package.
 - Some operations (i.e. `base_url` creation in `extract_async` or dict transformations in 
   `start_requests`) should be moved to a separate function and tested.
 - Modern crawlers add more complex behaviour (i.e. random delays between requests) to avoid being
   blocked by target servers.
