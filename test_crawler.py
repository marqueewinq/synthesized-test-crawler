import pytest
from tempfile import NamedTemporaryFile
from crawler import AsyncCrawler, write_dict_to_csv


@pytest.mark.parametrize(
    "html,base_url,expected",
    [
        ('sdf08<a attr=1 href="/e">sdf08i', "http://abc.def", ["http://abc.def/e"]),
        (
            'sdf08<a attr=1 href="/e/sdfg08ng/sdf">sdf08i',
            "http://abc.def",
            ["http://abc.def/e/sdfg08ng/sdf"],
        ),
        ("8f8<a attr=1>d8m8i", "http://abc.def", []),
    ],
)
def test_find_urls(html, base_url, expected):
    assert AsyncCrawler([], 0).find_urls(html, base_url) == expected


@pytest.mark.parametrize(
    "dic,csv", [([{"a": 1, "b": 3}, {"a": 2, "c": 2}], "a,b,c\r\n1,3,\r\n2,,2\r\n")]
)
def test_write_dict_to_csv(dic, csv):
    assert write_dict_to_csv(dic) == csv


def test_save_load():
    data = ["a", "b", "c"]
    current_depth = 1
    seen_urls = set(["a", "b"])
    results = ["abcosudngsodfuin"]
    pickle_path = NamedTemporaryFile().name

    crawler = AsyncCrawler(data, 0, pickle_path=pickle_path)
    crawler.seen_urls = seen_urls
    crawler.current_depth = current_depth
    crawler.results = results
    crawler.save()

    crawler = AsyncCrawler([], 0, pickle_path=pickle_path)
    crawler.load()
    assert crawler.to_fetch == data
    assert crawler.seen_urls == seen_urls
    assert crawler.current_depth == current_depth
    assert crawler.results == results
