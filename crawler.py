import fire
import os
from io import StringIO
import csv
import pickle
import asyncio
import aiohttp
import base64
from datetime import datetime
from urllib.parse import urljoin, urlparse
from lxml import html as lhtml


def str_to_base64(string):
    return base64.b64encode(string.encode("ascii"))


class AsyncCrawler:
    """Crawls the web pages asyncronously.

    The state is:

     - to_fetch : list of urls to fetch
     - seen_urls : set of already seen urls
     - current_depth : current depth (batch number) of crawling
     - results : fetched results (list of dicts)

    The action is:

     - Take to_fetch and perform fetches
     - Parse fetched pages and obtain new urls
     - Update the state:
        - to_fetch is set to new urls
        - increment the current_depth
        - update seen_urls and results accordingly.
    """

    def __init__(
        self,
        url_list,
        max_depth,
        max_concurrency=200,
        timeout=30,
        pickle_path="checkpoint.pickle",
    ):
        # parameters
        self.max_depth = max_depth
        self.pickle_path = pickle_path
        self.timeout = timeout
        # state
        self.to_fetch = list(url_list)
        self.seen_urls = set()
        self.current_depth = 0
        self.results = []
        # async utils
        self.session = aiohttp.ClientSession()
        self.semaphore = asyncio.BoundedSemaphore(max_concurrency)

    async def _http_request(self, url):
        """Makes http request.

        Args:

         - url : str

        Returns:

         - str containing html page or None in case request raised an exception.
        """
        async with self.semaphore:
            try:
                async with self.session.get(url, timeout=self.timeout) as response:
                    html = await response.read()
                    return str(html)
            except asyncio.TimeoutError as e:
                print(f"TimeoutError on {url}")
            except aiohttp.client_exceptions.InvalidURL as e:
                print(f"Invalid URL on {url}")
            except aiohttp.client_exceptions.TooManyRedirects as e:
                print(f"TooManyRedirects on {url}")

    def find_urls(self, html, base_url):
        """Extracts href links from html page.

        Extracted urls found in `self.seen_urls` are filtered out.

        Args:

         - html : str containing html content
         - base_url : str to join with found urls

        `base_url` is required, because frequently links on the web page are relative, so we
        must join them to create an actual url.

        Returns:

         - list of urls (as str)
        """
        found_urls = []
        dom = lhtml.fromstring(html)
        for href in dom.xpath("//a/@href"):
            url = urljoin(base_url, href)
            if url not in self.seen_urls and url.startswith(base_url):
                found_urls.append(url)
        return found_urls

    async def extract_async(self, url):
        """Performs a fetch on a single url.

        Returns:

         - dict containing:
            - `url` : original url
            - `data` : html page in base64 format
            - `found_urls` : list of urls on the page.
        """
        html_data = await self._http_request(url)
        if html_data is None:
            return None
        found_urls = set()
        if html_data:
            for found_url in self.find_urls(
                html_data, base_url=f"{urlparse(url).scheme}://{urlparse(url).netloc}"
            ):
                found_urls.add(found_url)
        return {
            "url": url,
            "data": str_to_base64(html_data),
            "found_urls": sorted(found_urls),
        }

    async def extract_multi_async(self, batch):
        """Performs a parallel fetches on a batch of urls.

        Args:

         - batch : list of urls

        Returns:

         - list of results from `extract_async`.
        """
        futures, results = [], []
        for url in batch:
            if url in self.seen_urls:
                continue
            futures.append(self.extract_async(url))

        for url, future in zip(batch, asyncio.as_completed(futures)):
            try:
                retval = await future
            except Exception as e:
                print(f"Unhandled exception: {e.name} {e}")
            if retval is not None:
                results.append(retval)
        return results

    async def start_requests(self):
        """Starting point for crawling.

        Executes fetches depth by depth and saves the crawler's state between each iteration.
        Fetched results are transformed into required format.

        Returns:

         - list of dicts as requested in pdf.
        """
        self.load()
        for depth in range(self.current_depth, self.max_depth + 1):
            # step forward
            batch_results = await self.extract_multi_async(self.to_fetch)
            # collect the update
            results = []
            new_to_fetch = []
            for item in batch_results:
                results.append(
                    {
                        "time": datetime.now(),
                        "domain": urlparse(item["url"]).netloc,
                        "url": item["url"],
                        "data": item["data"],
                    }
                )
                new_to_fetch.extend(item["found_urls"])
            # execute the update
            self.seen_urls.extend(self.to_fetch)
            self.to_fetch = new_to_fetch
            self.current_depth += 1
            self.results.extend(results)
            # commit
            self.save()

        await self.session.close()
        self.save()
        return self.results

    def save(self):
        if self.pickle_path is None:
            return
        pickle.dump(
            (self.to_fetch, self.seen_urls, self.current_depth, self.results),
            open(self.pickle_path, "wb"),
        )

    def load(self):
        if self.pickle_path is None:
            return
        if os.path.exists(self.pickle_path):
            self.to_fetch, self.seen_urls, self.current_depth, self.results = pickle.load(
                open(self.pickle_path, "rb")
            )


def write_dict_to_csv(indict):
    """Format the dict to csv string.

    Args:

     - indict: dict to transform

    Returns:

     - string in csv format
    """
    with StringIO() as csvfile:
        csvwriter = csv.writer(csvfile)
        keys = set()
        for item in indict:
            for key in item:
                keys.add(key)
        keys = sorted(list(keys))
        csvwriter.writerow(keys)
        for item in indict:
            csvwriter.writerow([item.get(key) for key in keys])
        return csvfile.getvalue()


def run_crawler(
    input_path, output_path, max_depth, pickle_path="checkpoint.pickle", timeout=30
):
    """Main entry point.

    Args:

     - `input_path` : path to a seedfile
     - `output_path` : path to write a csv file with results
     - `max_depth` : maximum link depth to crawl
     - `pickle_path` : path to a checkpoint file (removed after successful script run).
    """
    assert os.path.exists(input_path)
    assert max_depth >= 0

    url_list = open(input_path).read().split()
    crawler = AsyncCrawler(
        max_depth=max_depth, url_list=url_list, pickle_path=pickle_path, timeout=timeout
    )

    future = asyncio.Task(crawler.start_requests())
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(future)
    except KeyboardInterrupt as e:
        crawler.save()
        return
    loop.close()

    open(output_path, "w").write(write_dict_to_csv(future.result()))
    os.remove(pickle_path)


if __name__ == "__main__":
    fire.Fire(run_crawler)
